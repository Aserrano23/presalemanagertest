import { Injectable } from '@angular/core';
import Web3 from 'web3';

declare global {
  interface Window {
    ethereum: any;
  }
}

@Injectable({
  providedIn: 'root'
})
export class PresaleFactoryService {

  web3 = new Web3(window['ethereum']);
  
  contractAddress: string = '0x7026b3eBfa266B849bC120555cAD15748FccAcfD';
  abi = require('src/presale-factory-abi.json');
  contract = new this.web3.eth.Contract([]);
  
  constructor() { 
    this.contract = new this.web3.eth.Contract(this.abi, this.contractAddress);
    this.connectToMetamask();
  }

  async connectToMetamask(){
    await window['ethereum'].enable();

    window.ethereum.on('accountsChanged', function () {
      window.location.reload();
    });

    window.ethereum.on('chainChanged', function () {
      window.location.reload();
    });
  }
}
