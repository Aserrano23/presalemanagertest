import { Component, Input, OnInit } from '@angular/core';
import { PresaleFactoryService } from './presale-factory.service';
import { PresaleGeneratorService } from './presale-generator.service';
import { PresaleService } from './presale.service';
import { SaleTokenService } from './sale-token.service';

interface AllPresaleInfo{
  address: string,
  owner: string,
  saleToken: string,
  baseToken: string,
  tokenPrice: string,
  maxSpendPerBuyer: string,
  amount: string,
  hardCap: string,
  softCap: string,
  liquidityPercent: string,
  listingRate: string,
  startBlock: string,
  endBlock: string,
  lockPeriod: string,
  presaleInEth: boolean,
  status: number,
  whiteListOnly: boolean,
  lpGenerationComplete: boolean,
  forceFailed: boolean,
  totalBaseCollected: string,
  totalTokensSold: string,
  totalTokensWithdrawn: string,
  totalBaseWithdrawn: string,
  round1Length: string,
  numBuyers: string
}

interface PresaleInfo{
  address: string,
  owner: string,
  saleToken: string,
  baseToken: string,
  tokenPrice: string,
  maxSpendPerBuyer: string,
  amount: string,
  hardCap: string,
  softCap: string,
  liquidityPercent: string,
  listingRate: string,
  startBlock: string,
  endBlock: string,
  lockPeriod: string,
  presaleInEth: boolean
}

interface PresaleStatus{
  status: number,
  whiteListOnly: boolean,
  lpGenerationComplete: boolean,
  forceFailed: boolean,
  totalBaseCollected: string,
  totalTokensSold: string,
  totalTokensWithdrawn: string,
  totalBaseWithdrawn: string,
  round1Length: string,
  numBuyers: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'PresalePrueba';
  wallet: string = '';
  status!: number;
  timeToEnd: string = '';
  presaleLength: number = 0;
  lastPresale!: string;
  presalesAddresses!: string[];
  presalesInfo: PresaleInfo[] = Array<PresaleInfo>();
  presalesStatus: PresaleStatus[] = Array<PresaleStatus>();
  allPresaleInfo: AllPresaleInfo[] = Array<AllPresaleInfo>();

  @Input() amount!: number;
  @Input() tokenPrice!: number;
  @Input() maxEthPerBuyer!: number;
  @Input() hardCap!: number;
  @Input() softCap!: number;
  @Input() liquidityPercent!: number;
  @Input() listingRate!: number;
  @Input() startDate!: string;
  @Input() endDate!: string;
  @Input() lockPeriod!: number;

  @Input() quantity!: number;

  constructor(
    private generator: PresaleGeneratorService, 
    private presale: PresaleService,
    private factory: PresaleFactoryService,
    private saleToken: SaleTokenService
  ){

    this.generator.web3.eth.getAccounts().then(res => {
      this.wallet = res[0];
    });
  }

  ngOnInit(): void {
    this.getPresales();
  }
  
  async getPresalesInfoAndStatus(presales: string[]){
    for (let i = 0; i < presales.length; i++) {
      this.presale.contract.options.address = presales[i];
      await this.getPresaleStatus(presales[i]);
      
      await this.presale.contract.methods.PRESALE_INFO().call().then((res: any) => {
        this.presalesInfo[i] = {
          address: presales[i],
          owner: res.PRESALE_OWNER,
          saleToken: res.S_TOKEN,
          baseToken: res.B_TOKEN,
          tokenPrice: res.TOKEN_PRICE,
          maxSpendPerBuyer: res.MAX_SPEND_PER_BUYER,
          amount: res.AMOUNT,
          hardCap: res.HARDCAP,
          softCap: res.SOFTCAP,
          liquidityPercent: res.LIQUIDITY_PERCENT,
          listingRate: res.LISTING_RATE,
          startBlock: res.START_BLOCK,
          endBlock: res.END_BLOCK,
          lockPeriod: res.LOCK_PERIOD,
          presaleInEth: res.PRESALE_IN_ETH
        }
      });

      await this.presale.contract.methods.STATUS().call().then((res: any) => {
        this.presalesStatus[i] = {
          status: this.status,
          whiteListOnly: res.WHITELIST_ONLY,
          lpGenerationComplete: res.LP_GENERATION_COMPLETE,
          forceFailed: res.FORCE_FAILED,
          totalBaseCollected: res.TOTAL_BASE_COLLECTED,
          totalTokensSold: res.TOTAL_TOKENS_SOLD,
          totalTokensWithdrawn: res.TOTAL_TOKENS_WITHDRAWN,
          totalBaseWithdrawn: res.TOTAL_BASE_WITHDRAWN,
          round1Length: res.ROUND1_LENGTH,
          numBuyers: res.NUM_BUYERS
        }
      });
    }
    
    for (let i = 0; i < this.presaleLength; i++) {
      this.allPresaleInfo[i] = {
        address: this.presalesInfo[i].address,
        owner: this.presalesInfo[i].owner,
        saleToken: this.presalesInfo[i].saleToken,
        baseToken: this.presalesInfo[i].baseToken,
        tokenPrice: this.presalesInfo[i].tokenPrice,
        maxSpendPerBuyer: this.presalesInfo[i].maxSpendPerBuyer,
        amount: this.presalesInfo[i].amount,
        hardCap: this.presalesInfo[i].hardCap,
        softCap: this.presalesInfo[i].softCap,
        liquidityPercent: this.presalesInfo[i].liquidityPercent,
        listingRate: this.presalesInfo[i].listingRate,
        startBlock: this.presalesInfo[i].startBlock,
        endBlock: this.presalesInfo[i].endBlock,
        lockPeriod: this.presalesInfo[i].lockPeriod,
        presaleInEth: this.presalesInfo[i].presaleInEth,
        status: this.presalesStatus[i].status,
        whiteListOnly: this.presalesStatus[i].whiteListOnly,
        lpGenerationComplete: this.presalesStatus[i].lpGenerationComplete,
        forceFailed: this.presalesStatus[i].forceFailed,
        totalBaseCollected: this.presalesStatus[i].totalBaseCollected,
        totalTokensSold: this.presalesStatus[i].totalTokensSold,
        totalTokensWithdrawn: this.presalesStatus[i].totalTokensWithdrawn,
        totalBaseWithdrawn: this.presalesStatus[i].totalBaseWithdrawn,
        round1Length: this.presalesStatus[i].round1Length,
        numBuyers: this.presalesStatus[i].numBuyers
      }
        
      
    }
  }

  async getPresales(){
    await this.factory.contract.methods.presalesLength().call().then((res: any) => {
      this.presaleLength = res;
    });

    this.presalesAddresses = new Array(this.presaleLength);

    for (let i = 0; i < this.presaleLength; i++) {
      await this.factory.contract.methods.presaleAtIndex(i).call().then((res: any) => {
        this.presalesAddresses[i] = res;
      });
    }

    await this.getPresalesInfoAndStatus(this.presalesAddresses);
  }

  async getPresaleStatus(address: string){
    this.presale.contract.options.address = address;
    await this.presale.contract.methods.presaleStatus().call().then((res: any) => {
      this.status = res;
    });
  }
  
  getValueOfStatus(status: number, endBlock: string){
    if (status == 0) {
      return "Awaiting start block, you can't depositFailed, you can't deposit"
    }
    if (status == 1) {
      this.timeToEnd = "The presale ends at " + this.timestampToDate(this.blockToTimestamp(parseInt(endBlock)));
      return "Active, you can deposit"
    }
    if (status == 2) {
      return "Success, you can add liquidity but you can't deposit"
    }
    if (status == 3) {
      return "Failed, you can't deposit"
    }
    return ""
  }

  async userDeposit(){
    await this.factory.contract.methods.presaleAtIndex(this.presaleLength-1).call().then((res: any) => {
      this.presale.contract.options.address = res;
    });
    await this.saleToken.contract.methods.approve(this.generator.contractAddress, BigInt("115792089237316195423570985008687907853269984665640564039457584007913129639935")).send({from: this.wallet});
    await this.presale.contract.methods.userDeposit(BigInt(this.toWei(this.quantity))).send({from: this.wallet, value: this.toWei(this.quantity)});
  }

  async addLiquidity(){
    await this.presale.contract.methods.addLiquidity().send({from: this.wallet});
  }

  async createPresale(){
    await this.saleToken.contract.methods.approve(this.generator.contractAddress, BigInt("115792089237316195423570985008687907853269984665640564039457584007913129639935")).send({from: this.wallet});
    await this.generator.contract.methods.createPresale(
      this.wallet,
      "0x2380E0B9A297666f92E1Ed5416E1b342073cB25c",
      "0xd0A1E359811322d97991E03f863a0C30C2cF029C",
      "0x7bd2966267b9a946C1Bf9650AE82D8dcb23F32d6",
      [
        BigInt(this.toWei(this.amount)),
        BigInt(this.toWei(this.tokenPrice)),
        BigInt(this.toWei(this.maxEthPerBuyer)),
        BigInt(this.toWei(this.hardCap)),
        BigInt(this.toWei(this.softCap)),
        this.liquidityPercent,
        BigInt(this.toWei(this.listingRate)),
        this.TimestampToBlock(this.DateToTimestamp(this.startDate)),
        this.TimestampToBlock(this.DateToTimestamp(this.endDate)),
        this.lockPeriod
      ]
    ).send({from: this.wallet, value: 1000000000000000000});
  }

  toWei(ether: number){
    return ether * 1000000000000000000;
  }

  toEth(wei: string){
    return parseInt(wei) / 1000000000000000000;
  }

  DateToTimestamp(date: string){
    let datum = Date.parse(date);
    return datum/1000;
  }

  TimestampToBlock(timestamp: number){
    let exampleBlock: number = 25363630;
    let exampleTimestamp: number = 1623231356;

    let newTS: number = timestamp - exampleTimestamp;
    let blocks: number = newTS / 4;
    return (exampleBlock + blocks) - 1800;
  }

  blockToTimestamp(block: number){
    let exampleBlock: number = 25363630;
    let exampleTimestamp: number = 1623231356;

    let newBlock: number = block - exampleBlock;
    let secs: number = newBlock * 4;
    return exampleTimestamp + secs + 7200;
  }

  timestampToDate(timestamp: number){
    var a = new Date(timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
  }
}
