import { Injectable } from '@angular/core';
import Web3 from 'web3';

declare global {
  interface Window {
    ethereum: any;
  }
}

@Injectable({
  providedIn: 'root'
})
export class SaleTokenService {

  web3 = new Web3(window['ethereum']);
  
  contractAddress: string = '0x2380e0b9a297666f92e1ed5416e1b342073cb25c';
  abi = require('src/saleToken-abi.json');
  contract = new this.web3.eth.Contract([]);
  
  constructor() { 
    this.contract = new this.web3.eth.Contract(this.abi, this.contractAddress);
    this.connectToMetamask();
  }

  async connectToMetamask(){
    await window['ethereum'].enable();

    window.ethereum.on('accountsChanged', function () {
      window.location.reload();
    });

    window.ethereum.on('chainChanged', function () {
      window.location.reload();
    });
  }
}
