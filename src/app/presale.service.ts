import { Injectable } from '@angular/core';
import Web3 from 'web3';

declare global {
  interface Window {
    ethereum: any;
  }
}

@Injectable({
  providedIn: 'root'
})
export class PresaleService {

  web3 = new Web3(window['ethereum']);
  
  contractAddress: string = '0xe8b02f8E9E7A9749D1f4034ECbf8A261242bD1B8';
  abi = require('src/presale-abi.json');
  contract = new this.web3.eth.Contract([]);
  
  constructor() { 
    this.contract = new this.web3.eth.Contract(this.abi, this.contractAddress);
    this.connectToMetamask();
  }

  async connectToMetamask(){
    await window['ethereum'].enable();

    window.ethereum.on('accountsChanged', function () {
      window.location.reload();
    });

    window.ethereum.on('chainChanged', function () {
      window.location.reload();
    });
  }
}
